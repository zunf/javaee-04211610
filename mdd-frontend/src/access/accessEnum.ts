/**
 * 权限定义
 */
const ACCESS_ENUM = {
  NOT_LOGIN: "notLogin",
  USER: "user",
  ADMIN: "admin",
  BUSINESS: "business"
};

export default ACCESS_ENUM;
