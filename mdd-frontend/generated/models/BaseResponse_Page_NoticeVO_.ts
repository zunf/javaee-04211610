/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Page_NoticeVO_ } from './Page_NoticeVO_';

export type BaseResponse_Page_NoticeVO_ = {
    code?: number;
    data?: Page_NoticeVO_;
    message?: string;
};

