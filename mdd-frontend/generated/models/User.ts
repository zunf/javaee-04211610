/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type User = {
    address?: string;
    createTime?: string;
    id?: number;
    isDelete?: number;
    phone?: string;
    receiver?: string;
    updateTime?: string;
    userAccount?: string;
    userName?: string;
    userPassword?: string;
    userRole?: string;
};

