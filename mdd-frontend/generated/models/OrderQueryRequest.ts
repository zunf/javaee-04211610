/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrderQueryRequest = {
    current?: number;
    orderStatus?: number;
    pageSize?: number;
    productId?: number;
    sortField?: string;
    sortOrder?: string;
    userId?: number;
};

