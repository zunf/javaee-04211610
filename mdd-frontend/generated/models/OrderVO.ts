/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ProductVO } from './ProductVO';
import type { UserInfoVO } from './UserInfoVO';

export type OrderVO = {
    buyNum?: number;
    courierNum?: string;
    createTime?: string;
    id?: number;
    orderAmount?: number;
    orderStatus?: number;
    orderStatusStr?: string;
    product?: ProductVO;
    userInfo?: UserInfoVO;
};

