/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserInfoVO } from './UserInfoVO';

export type ProductVO = {
    createTime?: string;
    id?: number;
    price?: number;
    productName?: string;
    userInfo?: UserInfoVO;
};

