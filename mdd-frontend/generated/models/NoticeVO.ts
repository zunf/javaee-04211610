/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type NoticeVO = {
    createTime?: string;
    id?: number;
    message?: string;
    noticeType?: number;
    noticeTypeStr?: string;
    orderId?: number;
};

