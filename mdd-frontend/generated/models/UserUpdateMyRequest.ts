/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserUpdateMyRequest = {
    address?: string;
    id?: number;
    phone?: string;
    receiver?: string;
    userAccount?: string;
    userName?: string;
    userPassword?: string;
};

