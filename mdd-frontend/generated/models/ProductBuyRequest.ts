/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProductBuyRequest = {
    buyNum?: number;
    orderAmount?: number;
    productId?: number;
};

