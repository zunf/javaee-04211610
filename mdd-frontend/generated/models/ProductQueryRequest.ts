/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProductQueryRequest = {
    current?: number;
    id?: number;
    maxPrice?: number;
    minPrice?: number;
    pageSize?: number;
    productName?: string;
    sortField?: string;
    sortOrder?: string;
    userId?: number;
};

