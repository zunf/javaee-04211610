/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserRegisterRequest = {
    address?: string;
    checkPassword?: string;
    phone?: string;
    receiver?: string;
    userAccount?: string;
    userName?: string;
    userPassword?: string;
};

