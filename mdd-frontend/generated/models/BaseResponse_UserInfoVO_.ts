/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserInfoVO } from './UserInfoVO';

export type BaseResponse_UserInfoVO_ = {
    code?: number;
    data?: UserInfoVO;
    message?: string;
};

