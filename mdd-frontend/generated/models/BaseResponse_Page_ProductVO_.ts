/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Page_ProductVO_ } from './Page_ProductVO_';

export type BaseResponse_Page_ProductVO_ = {
    code?: number;
    data?: Page_ProductVO_;
    message?: string;
};

