/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { NoticeVO } from './NoticeVO';
import type { OrderItem } from './OrderItem';

export type Page_NoticeVO_ = {
    countId?: string;
    current?: number;
    maxLimit?: number;
    optimizeCountSql?: boolean;
    orders?: Array<OrderItem>;
    pages?: number;
    records?: Array<NoticeVO>;
    searchCount?: boolean;
    size?: number;
    total?: number;
};

