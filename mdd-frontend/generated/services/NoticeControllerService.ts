/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BaseResponse_Page_NoticeVO_ } from '../models/BaseResponse_Page_NoticeVO_';
import type { NoticeQueryRequest } from '../models/NoticeQueryRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class NoticeControllerService {

    /**
     * selectPageNotice
     * @param noticeQueryRequest noticeQueryRequest
     * @returns BaseResponse_Page_NoticeVO_ OK
     * @returns any Created
     * @throws ApiError
     */
    public static selectPageNoticeUsingPost(
        noticeQueryRequest: NoticeQueryRequest,
    ): CancelablePromise<BaseResponse_Page_NoticeVO_ | any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/notice/select/page',
            body: noticeQueryRequest,
            errors: {
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
            },
        });
    }

}
