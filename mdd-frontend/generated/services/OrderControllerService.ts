/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BaseResponse_boolean_ } from '../models/BaseResponse_boolean_';
import type { BaseResponse_Page_OrderVO_ } from '../models/BaseResponse_Page_OrderVO_';
import type { OrderCancelRequest } from '../models/OrderCancelRequest';
import type { OrderDeliverRequest } from '../models/OrderDeliverRequest';
import type { OrderQueryRequest } from '../models/OrderQueryRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class OrderControllerService {

    /**
     * cancelOrder
     * @param orderCancelRequest orderCancelRequest
     * @returns BaseResponse_boolean_ OK
     * @returns any Created
     * @throws ApiError
     */
    public static cancelOrderUsingPost(
        orderCancelRequest: OrderCancelRequest,
    ): CancelablePromise<BaseResponse_boolean_ | any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/order/cancel',
            body: orderCancelRequest,
            errors: {
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
            },
        });
    }

    /**
     * deliverOrder
     * @param orderDeliverRequest orderDeliverRequest
     * @returns BaseResponse_boolean_ OK
     * @returns any Created
     * @throws ApiError
     */
    public static deliverOrderUsingPost(
        orderDeliverRequest: OrderDeliverRequest,
    ): CancelablePromise<BaseResponse_boolean_ | any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/order/deliver',
            body: orderDeliverRequest,
            errors: {
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
            },
        });
    }

    /**
     * selectPageOrder
     * @param orderQueryRequest orderQueryRequest
     * @returns BaseResponse_Page_OrderVO_ OK
     * @returns any Created
     * @throws ApiError
     */
    public static selectPageOrderUsingPost(
        orderQueryRequest: OrderQueryRequest,
    ): CancelablePromise<BaseResponse_Page_OrderVO_ | any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/order/select/page',
            body: orderQueryRequest,
            errors: {
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
            },
        });
    }

}
