/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BaseResponse_boolean_ } from '../models/BaseResponse_boolean_';
import type { BaseResponse_Page_ProductVO_ } from '../models/BaseResponse_Page_ProductVO_';
import type { ProductAddRequest } from '../models/ProductAddRequest';
import type { ProductBuyRequest } from '../models/ProductBuyRequest';
import type { ProductQueryRequest } from '../models/ProductQueryRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ProductControllerService {

    /**
     * addProduct
     * @param productAddRequest productAddRequest
     * @returns BaseResponse_boolean_ OK
     * @returns any Created
     * @throws ApiError
     */
    public static addProductUsingPost(
        productAddRequest: ProductAddRequest,
    ): CancelablePromise<BaseResponse_boolean_ | any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/product/add',
            body: productAddRequest,
            errors: {
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
            },
        });
    }

    /**
     * buyProduct
     * @param productBuyRequest productBuyRequest
     * @returns BaseResponse_boolean_ OK
     * @returns any Created
     * @throws ApiError
     */
    public static buyProductUsingPost(
        productBuyRequest: ProductBuyRequest,
    ): CancelablePromise<BaseResponse_boolean_ | any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/product/buy',
            body: productBuyRequest,
            errors: {
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
            },
        });
    }

    /**
     * selectPageProduct
     * @param productQueryRequest productQueryRequest
     * @returns BaseResponse_Page_ProductVO_ OK
     * @returns any Created
     * @throws ApiError
     */
    public static selectPageProductUsingPost(
        productQueryRequest: ProductQueryRequest,
    ): CancelablePromise<BaseResponse_Page_ProductVO_ | any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/product/select/page',
            body: productQueryRequest,
            errors: {
                401: `Unauthorized`,
                403: `Forbidden`,
                404: `Not Found`,
            },
        });
    }

}
