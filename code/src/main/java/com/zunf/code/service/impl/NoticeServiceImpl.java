package com.zunf.code.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zunf.code.common.ErrorCode;
import com.zunf.code.enums.OrderStatusEnum;
import com.zunf.code.exception.BusinessException;
import com.zunf.code.mapper.NoticeMapper;
import com.zunf.code.model.dto.notice.NoticeQueryRequest;
import com.zunf.code.model.entity.Notice;
import com.zunf.code.enums.NoticeTypeEnum;
import com.zunf.code.model.entity.Order;
import com.zunf.code.model.entity.Product;
import com.zunf.code.model.entity.User;
import com.zunf.code.model.vo.notice.NoticeVO;
import com.zunf.code.service.NoticeService;
import com.zunf.code.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Resource
    private UserService userService;

    @Override
    public LambdaQueryWrapper<Notice> getQueryWrapper(NoticeQueryRequest noticeQueryRequest) {

        Long userId = noticeQueryRequest.getUserId();
        Integer noticeType = noticeQueryRequest.getNoticeType();

        LambdaQueryWrapper<Notice> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(userId != null, Notice::getUserId, userId);
        queryWrapper.eq(noticeType != null, Notice::getNoticeType, noticeType);
        return queryWrapper;

    }

    @Override
    public NoticeVO objTONoticeVO(Notice notice) {
        NoticeVO noticeVO = new NoticeVO();
        BeanUtils.copyProperties(notice, noticeVO);
        noticeVO.setNoticeTypeStr(NoticeTypeEnum.of(notice.getNoticeType()).getValue());
        return noticeVO;
    }

    @Override
    public boolean sendMessage(OrderStatusEnum orderStatus, User user, Product product, Integer buyNum , Order order) {

        StringBuilder message = new StringBuilder();
        message.append(user.getUserName());
        switch (orderStatus) {
            case PLACED:
                message.append("下单了");
                break;
            case CANCELLED:
                message.append("取消了");
                break;
            case DELIVERED:
                message.append("已发货");
                break;
            default:
                throw new BusinessException(ErrorCode.SYSTEM_ERROR, "暂不支持此类型");
        }
        message.append("商品：")
                .append(String.format("%s x %s，订单编号为：", product.getProductName(), buyNum))
                .append(order.getId());

        Notice notice = Notice.builder()
                .noticeType(NoticeTypeEnum.ORDER.getType())
                .orderId(order.getId())
                .message(message.toString()).build();

        User noticeUser = null;
        if (orderStatus == OrderStatusEnum.DELIVERED) {
            //发货操作，通知发送给下单用户
            noticeUser = userService.getById(order.getUserId());
        } else {
            //下单和取消操作，通知发送给商家
            noticeUser = userService.getById(product.getUserId());
        }
        notice.setUserRole(noticeUser.getUserRole());
        notice.setUserId(noticeUser.getId());

        return  this.save(notice);
    }
}




