package com.zunf.code.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zunf.code.enums.OrderStatusEnum;
import com.zunf.code.model.dto.notice.NoticeQueryRequest;
import com.zunf.code.model.entity.Notice;
import com.zunf.code.model.entity.Order;
import com.zunf.code.model.entity.Product;
import com.zunf.code.model.entity.User;
import com.zunf.code.model.vo.notice.NoticeVO;


public interface NoticeService extends IService<Notice> {

    LambdaQueryWrapper<Notice> getQueryWrapper(NoticeQueryRequest noticeQueryRequest);

    NoticeVO objTONoticeVO(Notice notice);

    boolean sendMessage(OrderStatusEnum orderStatus, User user, Product product, Integer buyNum, Order order);
}
