package com.zunf.code.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zunf.code.model.dto.product.ProductBuyRequest;
import com.zunf.code.model.dto.product.ProductQueryRequest;
import com.zunf.code.model.entity.Product;
import com.zunf.code.model.vo.product.ProductVO;

import javax.servlet.http.HttpServletRequest;


public interface ProductService extends IService<Product> {

    boolean isVaild(Product product);

    QueryWrapper<Product> getQueryWrapper(ProductQueryRequest productQueryRequest);

    ProductVO objTOProductVO(Product product);

    Boolean buyProduct(ProductBuyRequest productBuyRequest, HttpServletRequest request);

}
