package com.zunf.code.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zunf.code.enums.OrderStatusEnum;
import com.zunf.code.model.dto.order.OrderQueryRequest;
import com.zunf.code.model.entity.Order;
import com.zunf.code.model.vo.order.OrderVO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface OrderService extends IService<Order> {

    QueryWrapper<Order> getQueryWrapper(OrderQueryRequest orderQueryRequest);

    OrderVO objtoordervo(Order order);

    Boolean updateOrderStatusAndSendMessage(Map<String, Object> params, OrderStatusEnum orderStatus, HttpServletRequest request);
}
