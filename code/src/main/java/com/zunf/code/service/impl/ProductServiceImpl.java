package com.zunf.code.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zunf.code.common.CommonConstant;
import com.zunf.code.enums.OrderStatusEnum;
import com.zunf.code.mapper.ProductMapper;
import com.zunf.code.model.dto.product.ProductBuyRequest;
import com.zunf.code.model.dto.product.ProductQueryRequest;
import com.zunf.code.model.entity.Order;
import com.zunf.code.model.entity.Product;
import com.zunf.code.model.entity.User;
import com.zunf.code.model.vo.product.ProductVO;
import com.zunf.code.model.vo.user.UserInfoVO;
import com.zunf.code.service.NoticeService;
import com.zunf.code.service.OrderService;
import com.zunf.code.service.ProductService;
import com.zunf.code.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

    @Resource
    private UserService userService;

    @Resource
    private OrderService orderService;

    @Resource
    private NoticeService noticeService;

    @Override
    public boolean isVaild(Product product) {

        String productName = product.getProductName();
        BigDecimal price = product.getPrice();
        Long userId = product.getUserId();


        //商品名称最大字数为50
        if (StrUtil.isBlank(productName) || productName.length() > 50) return false;

        if (price == null) return false;

        if (userId == null) return false;

        return true;
    }

    @Override
    public QueryWrapper<Product> getQueryWrapper(ProductQueryRequest productQueryRequest) {
        Long id = productQueryRequest.getId();
        String productName = productQueryRequest.getProductName();
        BigDecimal minPrice = productQueryRequest.getMinPrice();
        BigDecimal maxPrice = productQueryRequest.getMaxPrice();
        Long userId = productQueryRequest.getUserId();
        String sortField = productQueryRequest.getSortField();
        String sortOrder = productQueryRequest.getSortOrder();

        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(id != null, "id", id);
        queryWrapper.like(StrUtil.isNotBlank(productName), "product_name", productName);
        queryWrapper.eq(userId != null, "user_id", userId);
        queryWrapper.between(minPrice != null, "minPrice", minPrice, maxPrice);
        queryWrapper.orderBy(StrUtil.isNotBlank(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC), sortField);
        return queryWrapper;
    }

    @Override
    public ProductVO objTOProductVO(Product product) {
        ProductVO productVO = new ProductVO();
        BeanUtils.copyProperties(product, productVO);
        UserInfoVO userInfoVo = userService.getUserInfoVo(userService.getById(product.getUserId()));
        productVO.setUserInfo(userInfoVo);
        return productVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean buyProduct(ProductBuyRequest productBuyRequest, HttpServletRequest request) {
        Long productId = productBuyRequest.getProductId();
        Product product = getById(productId);
        Integer buyNum = productBuyRequest.getBuyNum();
        double orderAmount = product.getPrice().doubleValue() * buyNum;
        User user = userService.getLoginUser(request);
        //添加用户的订单信息
        Order order = Order.builder().userId(user.getId()).productId(productId).buyNum(buyNum)
                .orderAmount(BigDecimal.valueOf(orderAmount))
                .orderStatus(OrderStatusEnum.PLACED.getType()).build();
        boolean saveOrder = orderService.save(order);

        //添加商家的订单消息通知
        boolean saveNotice = noticeService.sendMessage(OrderStatusEnum.PLACED, user, product, buyNum, order);

        return saveOrder && saveNotice;
    }
}




