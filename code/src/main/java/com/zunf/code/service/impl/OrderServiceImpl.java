package com.zunf.code.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zunf.code.common.CommonConstant;
import com.zunf.code.common.ErrorCode;
import com.zunf.code.enums.OrderStatusEnum;
import com.zunf.code.exception.BusinessException;
import com.zunf.code.mapper.OrderMapper;
import com.zunf.code.model.dto.order.OrderQueryRequest;
import com.zunf.code.model.entity.Order;
import com.zunf.code.model.entity.Product;
import com.zunf.code.model.entity.User;
import com.zunf.code.model.vo.order.OrderVO;
import com.zunf.code.service.NoticeService;
import com.zunf.code.service.OrderService;
import com.zunf.code.service.ProductService;
import com.zunf.code.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Resource
    @Lazy
    private ProductService productService;

    @Resource
    private UserService userService;

    @Resource
    private NoticeService noticeService;

    @Override
    public QueryWrapper<Order> getQueryWrapper(OrderQueryRequest orderQueryRequest) {

        Long userId = orderQueryRequest.getUserId();
        Long productId = orderQueryRequest.getProductId();
        Integer orderStatus = orderQueryRequest.getOrderStatus();
        String sortField = orderQueryRequest.getSortField();
        String sortOrder = orderQueryRequest.getSortOrder();

        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(userId != null, "user_id", userId);
        queryWrapper.eq(productId != null, "product_id", productId);
        queryWrapper.eq(orderStatus != null, "order_status", orderStatus);
        queryWrapper.orderBy(StrUtil.isNotBlank(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC), sortField);
        return queryWrapper;
    }

    @Override
    public OrderVO objtoordervo(Order order) {
        OrderVO orderVO = new OrderVO();
        BeanUtils.copyProperties(order, orderVO);
        orderVO.setOrderStatusStr(OrderStatusEnum.of(order.getOrderStatus()).getValue());
        orderVO.setProduct(productService.objTOProductVO(productService.getById(order.getProductId())));
        orderVO.setUserInfo(userService.getUserInfoVo(userService.getById(order.getUserId())));
        return orderVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateOrderStatusAndSendMessage(Map<String, Object> params, OrderStatusEnum orderStatus, HttpServletRequest request) {
        Long orderId = (Long) params.get("orderId");
        Order order = this.getById(orderId);
        // 如果是取消操作，判断是否已经发货，已发货不允许取消
        if (OrderStatusEnum.CANCELLED.equals(orderStatus)
                && OrderStatusEnum.DELIVERED.getType().equals(order.getOrderStatus())) {
            throw new BusinessException(ErrorCode.FORBIDDEN_ERROR, "商品已发货，不允许取消");
        }
        //如果是发货操作，判断是否已经取消
        if (OrderStatusEnum.DELIVERED.equals(orderStatus)
                && OrderStatusEnum.CANCELLED.getType().equals(order.getOrderStatus())) {
            throw new BusinessException(ErrorCode.FORBIDDEN_ERROR, "订单已取消，不允许发货");
        }
        //判断是否重复操作
        if (orderStatus == OrderStatusEnum.of(order.getOrderStatus())) {
            throw new BusinessException(ErrorCode.FORBIDDEN_ERROR, "请勿重复操作");
        }

        //更新订单状态
        boolean updateOrder = this.lambdaUpdate()
                .set(Order::getOrderStatus, orderStatus.getType())
                //如果是发货操作，更新快递单号
                .set(orderStatus == OrderStatusEnum.DELIVERED
                        , Order::getCourierNum, params.get("courierNum"))
                .eq(Order::getId, orderId)
                .update();
        Product product = productService.getById(order.getProductId());
        //当前操作的用户
        User user = userService.getLoginUser(request);

        //发送消息
        boolean saveNotice = noticeService
                .sendMessage(orderStatus, user, product, order.getBuyNum(), order);
        return updateOrder && saveNotice;
    }
}




