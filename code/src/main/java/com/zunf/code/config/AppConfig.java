package com.zunf.code.config;

import com.zunf.code.aop.LoggingAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class AppConfig {  
    @Bean
    public LoggingAspect loggingAspect() {
        return new LoggingAspect();  
    }  

}