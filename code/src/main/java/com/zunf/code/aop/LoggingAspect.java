package com.zunf.code.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;

@Aspect
@Slf4j
public class LoggingAspect {

    // 切入点：拦截com.example.service包下的所有类的所有方法
    @Pointcut("execution(* com.zunf.code.service.*.*(..))")
    public void serviceMethods() {}

    // 前置通知：在目标方法执行之前执行
    @Before("serviceMethods()")  
    public void beforeAdvice(JoinPoint joinPoint) {  
        log.info("Before method:" + joinPoint.getSignature().getName());
    }

    // 后置通知：在目标方法执行之后执行，无论方法是否抛出异常
    @After("serviceMethods()")  
    public void afterAdvice(JoinPoint joinPoint) {
        log.info("After method:" + joinPoint.getSignature().getName());
    }

    // 返回通知：在目标方法成功执行之后执行
    @AfterReturning(pointcut = "serviceMethods()", returning = "result")  
    public void afterReturningAdvice(JoinPoint joinPoint, Object result) {
        log.info("Method " + joinPoint.getSignature().getName() + " returns " + result);
    }

    // 异常通知：在目标方法抛出异常之后执行
    @AfterThrowing(pointcut = "serviceMethods()", throwing = "ex")  
    public void afterThrowingAdvice(JoinPoint joinPoint, Exception ex) {
        log.error("Exception in method " + joinPoint.getSignature().getName() + ": " + ex.toString());
    }  
}