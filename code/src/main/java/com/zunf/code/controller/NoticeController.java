package com.zunf.code.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zunf.code.common.BaseResponse;
import com.zunf.code.common.ErrorCode;
import com.zunf.code.model.dto.notice.NoticeQueryRequest;
import com.zunf.code.model.entity.Notice;
import com.zunf.code.model.vo.notice.NoticeVO;
import com.zunf.code.service.NoticeService;
import com.zunf.code.utils.ResultUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/notice")
public class NoticeController {

    @Resource
    private NoticeService noticeService;
    
    
    /**
     * 根据传入的查询条件查询数据
     *
     * @param noticeQueryRequest
     * @return 查询到符合条件的数据
     */
    @PostMapping("/select/page")
    public BaseResponse<Page<NoticeVO>> selectPageNotice(@RequestBody NoticeQueryRequest noticeQueryRequest) {
        if (noticeQueryRequest == null) {
            return ResultUtils.error(ErrorCode.PARAMS_ERROR);
        }

        int current = noticeQueryRequest.getCurrent();
        long pageSize = noticeQueryRequest.getPageSize();

        Page<Notice> page = noticeService.page(new Page<>(current, pageSize), noticeService.getQueryWrapper(noticeQueryRequest));

        Page<NoticeVO> result = new Page<>(current, pageSize, page.getTotal());
        result.setRecords(page.getRecords().stream().map(noticeService::objTONoticeVO).collect(Collectors.toList()));

        return ResultUtils.success(result);
    }
}
