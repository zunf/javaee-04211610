package com.zunf.code.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zunf.code.common.BaseResponse;
import com.zunf.code.common.ErrorCode;
import com.zunf.code.enums.OrderStatusEnum;
import com.zunf.code.exception.BusinessException;
import com.zunf.code.model.dto.order.OrderCancelRequest;
import com.zunf.code.model.dto.order.OrderDeliverRequest;
import com.zunf.code.model.dto.order.OrderQueryRequest;
import com.zunf.code.model.entity.Order;
import com.zunf.code.model.vo.order.OrderVO;
import com.zunf.code.service.OrderService;
import com.zunf.code.utils.ResultUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderService orderService;


    /**
     * 根据传入的查询条件查询数据
     *
     * @param orderQueryRequest
     * @return 查询到符合条件的数据
     */
    @PostMapping("/select/page")
    public BaseResponse<Page<OrderVO>> selectPageOrder(@RequestBody OrderQueryRequest orderQueryRequest) {
        if (orderQueryRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        int current = orderQueryRequest.getCurrent();
        long pageSize = orderQueryRequest.getPageSize();

        Page<Order> page = orderService.page(new Page<>(current, pageSize), orderService.getQueryWrapper(orderQueryRequest));

        Page<OrderVO> result = new Page<>(current, pageSize, page.getTotal());
        result.setRecords(page.getRecords().stream().map(orderService::objtoordervo).collect(Collectors.toList()));

        return ResultUtils.success(result);
    }

    /**
     * 取消订单
     * @param orderCancelRequest
     * @param request
     * @return
     */
    @PostMapping("/cancel")
    public BaseResponse<Boolean> cancelOrder(@RequestBody OrderCancelRequest orderCancelRequest, HttpServletRequest request) {
        if (orderCancelRequest == null || orderCancelRequest.getOrderId() == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Map<String, Object> params = BeanUtil.beanToMap(orderCancelRequest);
        Boolean isSuccess = orderService
                .updateOrderStatusAndSendMessage(params, OrderStatusEnum.CANCELLED, request);
        return ResultUtils.success(isSuccess);
    }

    /**
     * 发货订单
     * @param orderDeliverRequest
     * @param request
     * @return
     */
    @PostMapping("/deliver")
    public BaseResponse<Boolean> deliverOrder(@RequestBody OrderDeliverRequest orderDeliverRequest, HttpServletRequest request) {
        if (orderDeliverRequest == null || orderDeliverRequest.getOrderId() == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Map<String, Object> params = BeanUtil.beanToMap(orderDeliverRequest);
        Boolean updateSuccess = orderService
                .updateOrderStatusAndSendMessage(params, OrderStatusEnum.DELIVERED, request);
        return ResultUtils.success(updateSuccess);
    }

}
