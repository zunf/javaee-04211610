package com.zunf.code.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zunf.code.common.BaseResponse;
import com.zunf.code.common.ErrorCode;
import com.zunf.code.exception.BusinessException;
import com.zunf.code.model.dto.product.ProductAddRequest;
import com.zunf.code.model.dto.product.ProductBuyRequest;
import com.zunf.code.model.dto.product.ProductQueryRequest;
import com.zunf.code.model.entity.Product;
import com.zunf.code.model.vo.product.ProductVO;
import com.zunf.code.service.ProductService;
import com.zunf.code.utils.ResultUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Resource
    private ProductService productService;

    /**
     * 传入参数。添加商品
     *
     * @param productAddRequest
     * @return 是否添加成功
     */
    @PostMapping("/add")
    public BaseResponse<Boolean> addProduct(@RequestBody ProductAddRequest productAddRequest) {
        if (productAddRequest == null) {
            return ResultUtils.error(ErrorCode.PARAMS_ERROR);
        }

        Product product = new Product();

        BeanUtils.copyProperties(productAddRequest, product);

        if (!productService.isVaild(product)) return ResultUtils.error(ErrorCode.PARAMS_ERROR);

        boolean save = productService.save(product);

        return save ? ResultUtils.success(true) : ResultUtils.error(ErrorCode.SYSTEM_ERROR);
    }


    /**
     * 根据传入的查询条件查询数据
     *
     * @param productQueryRequest
     * @return 查询到符合条件的数据
     */
    @PostMapping("/select/page")
    public BaseResponse<Page<ProductVO>> selectPageProduct(@RequestBody ProductQueryRequest productQueryRequest) {
        if (productQueryRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        int current = productQueryRequest.getCurrent();
        long pageSize = productQueryRequest.getPageSize();

        Page<Product> page = productService.page(new Page<>(current, pageSize), productService.getQueryWrapper(productQueryRequest));

        Page<ProductVO> result = new Page<>(current, pageSize, page.getTotal());
        result.setRecords(page.getRecords().stream().map(productService::objTOProductVO).collect(Collectors.toList()));

        return ResultUtils.success(result);
    }

    /**
     * 购买商品
     *
     * @param productBuyRequest
     * @param request
     * @return
     */
    @PostMapping("/buy")
    public BaseResponse<Boolean> buyProduct(@RequestBody ProductBuyRequest productBuyRequest, HttpServletRequest request) {
        if (productBuyRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Boolean isSuccess = productService.buyProduct(productBuyRequest, request);
        return ResultUtils.success(isSuccess);
    }

}







