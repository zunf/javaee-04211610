package com.zunf.code.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Arrays;

@Getter
@AllArgsConstructor
@NoArgsConstructor
/**
 * 订单状态枚举
 *
 * @author zunf
 * @date 2024/5/29 14:06
 */
public enum OrderStatusEnum {

    /**
     * 订单状态：0为已下单/1为已发货/2为已取消
     */
    PLACED(0, "已下单"),
    DELIVERED(1, "已发货"),
    CANCELLED(2, "已取消");


    private Integer type;

    private String value;


    public static OrderStatusEnum of (Integer type) {
        return Arrays.stream(values()).filter(e -> e.getType().equals(type)).findFirst().orElse(null);
    }
}
