package com.zunf.code.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Arrays;

/**
 * 通知类型枚举
 *
 * @author zunf
 * @date 2024/5/28 16:15
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum NoticeTypeEnum {

    /**
     * 通知类型：0为订单通知/1为发货通知/2为取消通知
     */
    ORDER(0, "订单通知"),
    DELIVER(1, "发货通知"),
    CANCEL(2, "取消通知");


    private Integer type;

    private String value;


    public static NoticeTypeEnum of (Integer type) {
        return Arrays.stream(values()).filter(e -> e.getType().equals(type)).findFirst().orElse(null);
    }

}
