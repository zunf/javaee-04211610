package com.zunf.code.model.dto.product;

import com.zunf.code.common.PageRequest;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductQueryRequest extends PageRequest {

    /**
     * id
     */
    private Long id;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品单价-左边界
     */
    private BigDecimal minPrice;

    /**
     * 商品单价-右边界
     */
    private BigDecimal maxPrice;

    /**
     * 商家ID
     */
    private Long userId;
}
