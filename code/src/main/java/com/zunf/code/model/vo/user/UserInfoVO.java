package com.zunf.code.model.vo.user;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息VO
 * @author zunf
 */
@Data
public class UserInfoVO implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 账号
     */
    private String userAccount;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 收货地址
     */
    private String address;

    /**
     * 收货人
     */
    private String receiver;

    /**
     * 手机号
     */
    private String phone;

}
