package com.zunf.code.model.vo.order;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zunf.code.model.vo.product.ProductVO;
import com.zunf.code.model.vo.user.UserInfoVO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单VO
 *
 * @author zunf
 * @date 2024/5/28 14:37
 */
@Data
public class OrderVO implements Serializable {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户信息
     */
    private UserInfoVO userInfo;

    /**
     * 商品VO
     */
    private ProductVO product;

    /**
     * 购买数量
     */
    private Integer buyNum;

    /**
     * 订单金额
     */
    private BigDecimal orderAmount;

    /**
     * 订单状态：0为已下单/1为已发货/2为已取消
     */
    private Integer orderStatus;

    /**
     * 订单状态字符串
     */
    private String orderStatusStr;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 快递单号
     */
    private String courierNum;

    private static final long serialVersionUID = 5052166278440416595L;
}
