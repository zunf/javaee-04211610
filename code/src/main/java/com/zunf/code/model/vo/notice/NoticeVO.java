package com.zunf.code.model.vo.notice;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 通知VO
 *
 * @author zunf
 * @date 2024/5/28 14:15
 */
@Data
public class NoticeVO implements Serializable {

    /**
     * 通知编号
     */
    private Long id;

    /**
     * 通知类型：0为订单通知/1为发货通知/2为取消通知
     */
    private Integer noticeType;

    /**
     * 通知类型字符串
     */
    private String noticeTypeStr;

    /**
     * 通知内容
     */
    private String message;

    /**
     * 订单编号
     */
    private Long orderId;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private static final long serialVersionUID = -4044770327639597191L;
}
