package com.zunf.code.model.dto.user;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册请求体
 * @author zunf
 */
@Data
public class UserRegisterRequest implements Serializable {

    private static final long serialVersionUID = 3191241716373120793L;

    /**
     * 账号
     */
    private String userAccount;

    /**
     * 密码
     */
    private String userPassword;

    /**
     * 确认密码
     */
    private String checkPassword;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 收货地址
     */
    private String address;

    /**
     * 收货人
     */
    private String receiver;

    /**
     * 手机号
     */
    private String phone;


}
