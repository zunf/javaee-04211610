package com.zunf.code.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 通知表
 * @TableName notice
 */
@TableName(value ="notice")
@Data
@Builder
public class Notice implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 接收方角色：user/business
     */
    private String userRole;

    /**
     * 接收方ID
     */
    private Long userId;

    /**
     * 通知类型：0为订单通知/1为发货通知/2为取消通知
     */
    private Integer noticeType;

    /**
     * 通知内容
     */
    private String message;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;


    /**
     * 订单编号
     */
    private Long orderId;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDelete;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}