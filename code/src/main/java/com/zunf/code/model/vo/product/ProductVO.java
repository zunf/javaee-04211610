package com.zunf.code.model.vo.product;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zunf.code.model.vo.user.UserInfoVO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *  商品VO
 *
 * @author zunf
 * @date 2024/5/28 11:06
 */
@Data
public class ProductVO implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 用户信息
     */
    private UserInfoVO userInfo;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private static final long serialVersionUID = 3725374003773119502L;

}
