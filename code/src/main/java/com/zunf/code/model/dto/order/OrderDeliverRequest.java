package com.zunf.code.model.dto.order;

import lombok.Data;

/**
 * 订单发货请求参数
 *
 * @author zunf
 * @date 2024/5/29 15:03
 */
@Data
public class OrderDeliverRequest {
    /**
     * 订单编号
     */
    private Long orderId;

    /**
     * 快递单号
     */
    private String courierNum;
}
