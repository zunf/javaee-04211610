package com.zunf.code.model.dto.notice;

import com.zunf.code.common.PageRequest;
import lombok.Data;

import java.io.Serializable;

/**
 * 通知查询请求参数
 *
 * @author zunf
 * @date 2024/5/28 14:12
 */
@Data
public class NoticeQueryRequest extends PageRequest implements Serializable{

    /**
     * 接收方ID
     */
    private Long userId;

    /**
     * 通知类型：0为订单通知/1为发货通知/2为取消通知
     */
    private Integer noticeType;

    private static final long serialVersionUID = 2576185314935314502L;
}
