package com.zunf.code.model.dto.order;

import lombok.Data;

/**
 * 订单取消请求参数
 *
 * @author zunf
 * @date 2024/5/29 14:13
 */
@Data
public class OrderCancelRequest {

    /**
     * 订单编号
     */
    private Long orderId;
}
