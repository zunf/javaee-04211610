package com.zunf.code.model.dto.product;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductAddRequest {

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 商家ID
     */
    private Long userId;

}
