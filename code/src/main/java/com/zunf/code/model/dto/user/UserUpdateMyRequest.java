package com.zunf.code.model.dto.user;

import lombok.Data;

import java.io.Serializable;

/**
 * 更新个人信息请求参数
 *
 * @author zunf
 */
@Data
public class UserUpdateMyRequest implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 账号
     */
    private String userAccount;

    /**
     * 密码
     */
    private String userPassword;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 收货地址
     */
    private String address;

    /**
     * 收货人
     */
    private String receiver;

    /**
     * 手机号
     */
    private String phone;

}
