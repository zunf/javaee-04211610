package com.zunf.code.model.dto.product;

import lombok.Data;

@Data
public class ProductBuyRequest {

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 购买数量
     */
    private Integer buyNum;

    /**
     * 订单总金额
     */
    private double orderAmount;
}
