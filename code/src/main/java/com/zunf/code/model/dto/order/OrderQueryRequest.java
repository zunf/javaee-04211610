package com.zunf.code.model.dto.order;

import com.zunf.code.common.PageRequest;
import lombok.Data;

import java.io.Serializable;

/**
 * 订单信息请求参数
 *
 * @author zunf
 * @date 2024/5/28 14:33
 */
@Data
public class OrderQueryRequest extends PageRequest implements Serializable {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 订单状态：0为已下单/1为已发货/2为已取消
     */
    private Integer orderStatus;

    private static final long serialVersionUID = 8529496934393938260L;
}
