package com.zunf.code.model.dto.user;

import com.zunf.code.common.PageRequest;
import lombok.Data;

/**
 * 查询用户列表
 *
 * @author zunf
 */
@Data
public class UserQueryRequest extends PageRequest {
    /**
     * id
     */
    private Long id;

    /**
     * 账号
     */
    private String userAccount;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户角色：user/business/ban
     */
    private String userRole;
}
