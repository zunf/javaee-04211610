package com.zunf.code.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zunf.code.model.dto.user.UserQueryRequest;
import com.zunf.code.model.entity.User;
import com.zunf.code.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class UserControllerTest {

    @Resource
    private UserService userService;

    @Test
    void listPictureByPage() {
        UserQueryRequest userQueryRequest = new UserQueryRequest();
        int current = userQueryRequest.getCurrent();
        long pageSize = userQueryRequest.getPageSize();
        Page<User> userPage = userService.page(new Page<>(current, pageSize), userService.getQueryWrapper(userQueryRequest));
        System.out.println(JSONUtil.toJsonStr(userPage));
    }


}