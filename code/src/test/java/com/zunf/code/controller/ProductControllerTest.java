package com.zunf.code.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zunf.code.common.ErrorCode;
import com.zunf.code.model.dto.product.ProductAddRequest;
import com.zunf.code.model.dto.product.ProductQueryRequest;
import com.zunf.code.model.entity.Product;
import com.zunf.code.service.ProductService;
import com.zunf.code.utils.ResultUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.math.BigDecimal;

@SpringBootTest
public class ProductControllerTest {

    @Resource
    private ProductService productService;

    @Test
    void addProduct() {
        ProductAddRequest productAddRequest = new ProductAddRequest();
        productAddRequest.setProductName("洗发水");
        productAddRequest.setPrice(new BigDecimal("12.1"));
        productAddRequest.setUserId(12323123L);


        if (productAddRequest == null) {
            System.out.println(ResultUtils.error(ErrorCode.PARAMS_ERROR));
            return;
        }

        Product product = new Product();

        BeanUtils.copyProperties(productAddRequest, product);

        if (!productService.isVaild(product)) {
            System.out.println( ResultUtils.error(ErrorCode.PARAMS_ERROR));
            return;
        }

        boolean save = productService.save(product);

        System.out.println(save ? ResultUtils.success(true) : ResultUtils.error(ErrorCode.SYSTEM_ERROR));
    }

    @Test
    void selectPageProduct() {
        ProductQueryRequest productQueryRequest = new ProductQueryRequest();

        if (productQueryRequest == null) System.out.println(ResultUtils.error(ErrorCode.PARAMS_ERROR));

        int current = productQueryRequest.getCurrent();
        long pageSize = productQueryRequest.getPageSize();

        Page<Product> page = productService.page(new Page<>(current, pageSize), productService.getQueryWrapper(productQueryRequest));

        System.out.println(ResultUtils.success(page));
    }
}
