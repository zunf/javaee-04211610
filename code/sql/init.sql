-- 创建数据库
create database if not exists mdd_db;

-- 切换数据库
use mdd_db;

-- 用户表
create table if not exists `user`
(
    id            bigint auto_increment comment 'id' primary key,
    user_account  varchar(256)                             not null comment '账号',
    user_password varchar(512)                             not null comment '密码',
    user_name     varchar(256)                             null comment '用户名称',
    address       varchar(1024)                            null comment '收货地址',
    receiver      varchar(256)                             null comment '收货人',
    phone         varchar(256)                             null comment '手机号',
    user_role     varchar(256) default 'user'              not null comment '用户角色：user/business/ban',
    create_time   datetime     default (CURRENT_TIMESTAMP) not null comment '创建时间',
    update_time   datetime     default (CURRENT_TIMESTAMP) not null on update CURRENT_TIMESTAMP comment '更新时间',
    is_delete     tinyint      default 0                   not null comment '是否删除',
    index idx_account_pswd (user_account, user_password)
) comment '用户表' collate = utf8mb4_unicode_ci;


-- 商品表
create table if not exists product
(
    id           bigint auto_increment comment 'id' primary key,
    product_name varchar(256)                         not null comment '商品名称',
    price        decimal(6, 2)                        not null comment '商品单价',
    user_id      bigint                               not null COMMENT '商家ID',
    create_time  datetime default (CURRENT_TIMESTAMP) not null comment '创建时间',
    update_time  datetime default (CURRENT_TIMESTAMP) not null on update CURRENT_TIMESTAMP comment '更新时间',
    is_delete    tinyint  default 0                   not null comment '是否删除',
    index idx_userid (user_id)
) comment '商品表' collate = utf8mb4_unicode_ci;


-- 订单表
create table if not exists `order`
(
    id           bigint auto_increment comment 'id' primary key,
    user_id      bigint                               not null comment '用户ID',
    product_id   bigint                               not null comment '商品ID',
    buy_num      int                                  not null comment '购买数量',
    order_amount decimal(7, 2)                        not null comment '订单金额',
    order_status tinyint  default 0                   not null comment '订单状态：0为已下单/1为已发货/2为已取消',
    courier_Num  varchar(1024)                        null     comment '快递单号',
    create_time  datetime default (CURRENT_TIMESTAMP) not null comment '创建时间',
    update_time  datetime default (CURRENT_TIMESTAMP) not null on update CURRENT_TIMESTAMP comment '更新时间',
    is_delete    tinyint  default 0                   not null comment '是否删除',
    index idx_userid (user_id)
) comment '订单表' collate = utf8mb4_unicode_ci;


-- 通知表
create table if not exists `notice`
(
    id          bigint auto_increment comment 'id' primary key,
    user_role   varchar(256)                         not null comment '接收方角色：user/business',
    user_id     bigint                               not null comment '接收方ID',
    notice_type tinyint                              not null comment '通知类型：0为订单通知/1为发货通知/2为取消通知',
    message     varchar(2048)                        not null comment '通知内容',
    order_id    bigint                               null     comment '订单编号',
    create_time datetime default (CURRENT_TIMESTAMP) not null comment '创建时间',
    update_time datetime default (CURRENT_TIMESTAMP) not null on update CURRENT_TIMESTAMP comment '更新时间',
    is_delete   tinyint  default 0                   not null comment '是否删除',
    index idx_userid (user_id)
) comment '通知表' collate = utf8mb4_unicode_ci;